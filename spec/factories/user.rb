FactoryBot.define do
  factory :user do
    name Faker::Name.first_name
    surname Faker::Name.last_name
    email Faker::Internet.email
    password "ADMINPSWD"
    password_confirmation "ADMINPSWD"
    confirmed_at Time.zone.now
  end
end
